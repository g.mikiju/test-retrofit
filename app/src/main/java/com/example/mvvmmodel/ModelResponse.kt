package com.example.mvvmmodel

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class ModelResponse() : Parcelable {

    @SerializedName("userId")
    var userId: Int? = null

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("completed")
    var completed: Boolean? = null

    constructor(parcel: Parcel) : this() {
        userId = parcel.readValue(Int::class.java.classLoader) as? Int
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        title = parcel.readString()
        completed = parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(userId)
        parcel.writeValue(id)
        parcel.writeString(title)
        parcel.writeValue(completed)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelResponse> {
        override fun createFromParcel(parcel: Parcel): ModelResponse {
            return ModelResponse(parcel)
        }

        override fun newArray(size: Int): Array<ModelResponse?> {
            return arrayOfNulls(size)
        }
    }

}